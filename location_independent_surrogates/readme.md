# Deep temporal convolutional network to estimate building energy performance for all Canadian Climates.

1D convolutional neural networks have been applied to classify various types of time series. In this study, we leverage them to process annual hourly weather data into a few latent variables. These variables serve as input to a building simulation surrogate model (vanilla ANN) to predict building performance, e.g. energy demand or carbon emissions, for any climate. In this study, we showcase the use for Canadian climates only. 


![](Pics/method_overview.png)



## Contact:

Paul Westermann, Kevin Cant, Ralph Evins [Energy in Cities group, University of Victoria, Canada](https://energyincities.gitlab.io/website/)

<img src="Pics/uvic_logo.jpg" alt="drawing" width="200"/>

#### Acknowledgements:
Thanks to Matthias Welzel for project-specific code development; Gaby Baasch for discussion; and Dylan Kemp and Paul Kovacs for maintaing the [BESOS platform](https://besos.uvic.ca/) (where we ran the majority of our experiments).