 ! The following Location and Design Day data are produced as possible from the weather source data.
 ! Wind Speeds follow the traditional values (6.7 m/s heating, 3.35 m/s cooling)
 ! No special attempts at re-creating or determining missing data parts (e.g. Wind speed or direction)
 ! are done.  Therefore, you should look at the data and fill in any incorrect values as you desire.
 
 ! SizingPeriod:DesignDay and other objects are formatted to meet criteria of EnergyPlus V7.1 and later
 
 ! Some users have indicated that they would like the actual day of week from the weather file to be
 ! used as the start day of week for a RunPeriod even though this is relatively meaningless for
 ! TMY data files as future months will not use their start days of week (simulations need to show
 ! continuity in day of week presentation).
 
 ! In an effort to accomodate, the following initial month days are included:
 ! Note JAN 1, 2012 is a Sunday
 ! Note FEB 1, 2018 is a Thursday
 ! Note MAR 1, 2015 is a Sunday
 ! Note APR 1, 2017 is a Saturday
 ! Note MAY 1, 2014 is a Thursday
 ! Note JUN 1, 2018 is a Friday
 ! Note JUL 1, 2017 is a Saturday
 ! Note AUG 1, 2012 is a Wednesday
 ! Note SEP 1, 2013 is a Sunday
 ! Note OCT 1, 2012 is a Monday
 ! Note NOV 1, 2016 is a Tuesday
 ! Note DEC 1, 2014 is a Monday
 
 ! Since the RunPeriod object changes in V9.0, the following are RunPeriod objects in comments:
 ! Version before EnergyPlus release V9.0
 !RunPeriod,
 !Weather Data,    !- Name
 !  1,             !- Begin Month
 !  1,             !- Begin Day of Month
 !  12,            !- End Month
 !  31,            !- End Day of Month
 !Sunday,      !- Day of Week for Start Day
 !  No,            !- Use Weather File Holidays and Special Days
 !  No,            !- Use Weather File Daylight Saving Period
 ! Yes,            !- Apply Weekend Holiday Rule
 ! Yes,            !- Use Weather File Rain Indicators
 ! Yes;            !- Use Weather File Snow Indicators
 
 ! Version EnergyPlus release V9.0+
 !RunPeriod,
 !Weather Data,    !- Name
 !  1,             !- Begin Month
 !  1,             !- Begin Day of Month
 !   ,             !- Begin Year
 !  12,            !- End Month
 !  31,            !- End Day of Month
 !   ,             !- End Year
 !Sunday,      !- Day of Week for Start Day
 !  No,            !- Use Weather File Holidays and Special Days
 !  No,            !- Use Weather File Daylight Saving Period
 ! Yes,            !- Apply Weekend Holiday Rule
 ! Yes,            !- Use Weather File Rain Indicators
 ! Yes,            !- Use Weather File Snow Indicators
 ! ;               !- Treat Weather as Acual
 
 Site:Location,
  Fribourg Posieux_FR_CHE Design_Conditions,     !- Location Name
      46.77,     !- Latitude {N+ S-}
       7.11,     !- Longitude {W- E+}
       1.00,     !- Time Zone Relative to GMT {GMT+/-}
     647.60;     !- Elevation {m}
  
 ! The following Sizing Period objects for Extreme and Typical conditions are calculated
 ! from the extreme (if any) and typical conditions on the weather source data.
 ! The actual weeks that will be used will exist on the weather file.
  
 SizingPeriod:WeatherFileConditionType,
   Summer Extreme,
   SummerExtreme,
   SummerDesignDay,
   Yes,                !- Use Weather File Daylight Saving Period
   Yes;                !- Use Weather File Rain and Snow Indicators
 SizingPeriod:WeatherFileConditionType,
   Summer Typical,
   SummerTypical,
   Monday,
   Yes,                !- Use Weather File Daylight Saving Period
   Yes;                !- Use Weather File Rain and Snow Indicators
 SizingPeriod:WeatherFileConditionType,
   Winter Extreme,
   WinterExtreme,
   WinterDesignDay,
   Yes,                !- Use Weather File Daylight Saving Period
   Yes;                !- Use Weather File Rain and Snow Indicators
 SizingPeriod:WeatherFileConditionType,
   Winter Typical,
   WinterTypical,
   Monday,
   Yes,                !- Use Weather File Daylight Saving Period
   Yes;                !- Use Weather File Rain and Snow Indicators
 SizingPeriod:WeatherFileConditionType,
   Autumn Typical,
   AutumnTypical,
   Monday,
   Yes,                !- Use Weather File Daylight Saving Period
   Yes;                !- Use Weather File Rain and Snow Indicators
 SizingPeriod:WeatherFileConditionType,
   Spring Typical,
   SpringTypical,
   Monday,
   Yes,                !- Use Weather File Daylight Saving Period
   Yes;                !- Use Weather File Rain and Snow Indicators
 


 ! Site:Precipitation. RoofIrrigation and Schedule:File objects for rainfall
 ! Data is built from rainfall data on the source data
  Site:Precipitation,  
    ScheduleAndDesignLevel,  !- Precipitation Model Type
    0.951,  !- Design Level for Total Annual Preciptation
    Rainfall Data from EPW file,    !- Precipitation Rates Schedule Name
    0.951;  !- Average Total Annual Precipitation
 
  RoofIrrigation,  
    Schedule,  !- Irridation Model Type
    Rainfall Data from EPW file,    !- Precipitation Rates Schedule Name
    ;  !- Irrigation Maximum Saturation Threshold (default used)
 
  Schedule:File, 
    Rainfall Data from EPW file,  !- Name
    ,                       !- Schedule Type Limits Name
    CHE_FR_Fribourg.Posieux.066250_TMYx.rain,    !- File Name
    1,                       !- Column Number
    1,                       !- Rows to Skip at Top
    8760;    !- Number of Hours of Data


 ! No Design Conditions found for this Location
 ! Following definitions are created from weather source data
 ! Fribourg Posieux_FR_CHE Annual Heating 99.6%, MaxDB=-12.3C
 SizingPeriod:DesignDay,
  Fribourg Posieux Ann Htg 99.6% Condns DB,     !- Name
          2,      !- Month
         21,      !- Day of Month
  WinterDesignDay,!- Day Type
      -12.3,      !- Maximum Dry-Bulb Temperature {C}
        0.0,      !- Daily Dry-Bulb Temperature Range {C}
 DefaultMultipliers, !- Dry-Bulb Temperature Range Modifier Type
           ,      !- Dry-Bulb Temperature Range Modifier Day Schedule Name
    Wetbulb,      !- Humidity Condition Type
      -12.3,      !- Wetbulb at Maximum Dry-Bulb {C}
           ,      !- Humidity Indicating Day Schedule Name
           ,      !- Humidity Ratio at Maximum Dry-Bulb {kgWater/kgDryAir}
           ,      !- Enthalpy at Maximum Dry-Bulb {J/kg}
           ,      !- Daily Wet-Bulb Temperature Range {deltaC}
     93783.,      !- Barometric Pressure {Pa}
       6.71,!- [N/A] Wind Speed {m/s} traditional 6.71 m/s (15 mph)
          0,   !- [N/A] Wind Direction {Degrees; N=0, S=180}
         No,      !- Rain {Yes/No}
         No,      !- Snow on ground {Yes/No}
         No,      !- Daylight Savings Time Indicator
  ASHRAEClearSky, !- Solar Model Indicator
           ,      !- Beam Solar Day Schedule Name
           ,      !- Diffuse Solar Day Schedule Name
           ,      !- ASHRAE Clear Sky Optical Depth for Beam Irradiance (taub)
           ,      !- ASHRAE Clear Sky Optical Depth for Diffuse Irradiance (taud)
       0.00;      !- Clearness {0.0 to 1.1}
 
 ! Fribourg Posieux_FR_CHE Annual Heating 99%, MaxDB=-11.3C
 SizingPeriod:DesignDay,
  Fribourg Posieux Ann Htg 99% Condns DB,     !- Name
          2,      !- Month
         21,      !- Day of Month
  WinterDesignDay,!- Day Type
      -11.3,      !- Maximum Dry-Bulb Temperature {C}
        0.0,      !- Daily Dry-Bulb Temperature Range {C}
 DefaultMultipliers, !- Dry-Bulb Temperature Range Modifier Type
           ,      !- Dry-Bulb Temperature Range Modifier Day Schedule Name
    Wetbulb,      !- Humidity Condition Type
      -11.3,      !- Wetbulb at Maximum Dry-Bulb {C}
           ,      !- Humidity Indicating Day Schedule Name
           ,      !- Humidity Ratio at Maximum Dry-Bulb {kgWater/kgDryAir}
           ,      !- Enthalpy at Maximum Dry-Bulb {J/kg}
           ,      !- Daily Wet-Bulb Temperature Range {deltaC}
     93783.,      !- Barometric Pressure {Pa}
       6.71,!- [N/A] Wind Speed {m/s} traditional 6.71 m/s (15 mph)
          0,   !- [N/A] Wind Direction {Degrees; N=0, S=180}
         No,      !- Rain {Yes/No}
         No,      !- Snow on ground {Yes/No}
         No,      !- Daylight Savings Time Indicator
  ASHRAEClearSky, !- Solar Model Indicator
           ,      !- Beam Solar Day Schedule Name
           ,      !- Diffuse Solar Day Schedule Name
           ,      !- ASHRAE Clear Sky Optical Depth for Beam Irradiance (taub)
           ,      !- ASHRAE Clear Sky Optical Depth for Diffuse Irradiance (taud)
       0.00;      !- Clearness {0.0 to 1.1}
 
 ! Fribourg Posieux_FR_CHE Annual Cooling (DP=>MDB) .4%, MDB=30.5C DP=18.6C
 SizingPeriod:DesignDay,
  Fribourg Posieux Ann Clg .4% Condns DP=>MDB,     !- Name
          8,      !- Month
         21,      !- Day of Month
  SummerDesignDay,!- Day Type
       30.5,      !- Maximum Dry-Bulb Temperature {C}
       10.2,      !- Daily Dry-Bulb Temperature Range {C}
 DefaultMultipliers, !- Dry-Bulb Temperature Range Modifier Type
           ,      !- Dry-Bulb Temperature Range Modifier Day Schedule Name
    Dewpoint,     !- Humidity Condition Type
       18.6,      !- Dewpoint at Maximum Dry-Bulb {C}
           ,      !- Humidity Indicating Day Schedule Name
           ,      !- Humidity Ratio at Maximum Dry-Bulb {kgWater/kgDryAir}
           ,      !- Enthalpy at Maximum Dry-Bulb {J/kg}
           ,      !- Daily Wet-Bulb Temperature Range {deltaC}
     93783.,      !- Barometric Pressure {Pa}
       3.35,   !- [N/A] Wind Speed {m/s} traditional 3.35 m/s (7mph)
          0,   !- [N/A] Wind Direction {Degrees; N=0, S=180}
         No,      !- Rain {Yes/No}
         No,      !- Snow on ground {Yes/No}
         No,      !- Daylight Savings Time Indicator
  ASHRAEClearSky, !- Solar Model Indicator
           ,      !- Beam Solar Day Schedule Name
           ,      !- Diffuse Solar Day Schedule Name
           ,      !- ASHRAE Clear Sky Optical Depth for Beam Irradiance (taub)
           ,      !- ASHRAE Clear Sky Optical Depth for Diffuse Irradiance (taud)
       1.00;      !- Clearness {0.0 to 1.1}
 
 ! Fribourg Posieux_FR_CHE Annual Cooling (DP=>MDB) 1%, MDB=29.6C DP=18.2C
 SizingPeriod:DesignDay,
  Fribourg Posieux Ann Clg 1% Condns DP=>MDB,     !- Name
          8,      !- Month
         21,      !- Day of Month
  SummerDesignDay,!- Day Type
       29.6,      !- Maximum Dry-Bulb Temperature {C}
       10.2,      !- Daily Dry-Bulb Temperature Range {C}
 DefaultMultipliers, !- Dry-Bulb Temperature Range Modifier Type
           ,      !- Dry-Bulb Temperature Range Modifier Day Schedule Name
    Dewpoint,     !- Humidity Condition Type
       18.2,      !- Dewpoint at Maximum Dry-Bulb {C}
           ,      !- Humidity Indicating Day Schedule Name
           ,      !- Humidity Ratio at Maximum Dry-Bulb {kgWater/kgDryAir}
           ,      !- Enthalpy at Maximum Dry-Bulb {J/kg}
           ,      !- Daily Wet-Bulb Temperature Range {deltaC}
     93783.,      !- Barometric Pressure {Pa}
       3.35,   !- [N/A] Wind Speed {m/s} traditional 3.35 m/s (7mph)
          0,   !- [N/A] Wind Direction {Degrees; N=0, S=180}
         No,      !- Rain {Yes/No}
         No,      !- Snow on ground {Yes/No}
         No,      !- Daylight Savings Time Indicator
  ASHRAEClearSky, !- Solar Model Indicator
           ,      !- Beam Solar Day Schedule Name
           ,      !- Diffuse Solar Day Schedule Name
           ,      !- ASHRAE Clear Sky Optical Depth for Beam Irradiance (taub)
           ,      !- ASHRAE Clear Sky Optical Depth for Diffuse Irradiance (taud)
       1.00;      !- Clearness {0.0 to 1.1}
 
 ! Fribourg Posieux_FR_CHE Annual Cooling (DP=>MDB) 2%, MDB=28.4C DP=17.8C
 SizingPeriod:DesignDay,
  Fribourg Posieux Ann Clg 2% Condns DP=>MDB,     !- Name
          8,      !- Month
         21,      !- Day of Month
  SummerDesignDay,!- Day Type
       28.4,      !- Maximum Dry-Bulb Temperature {C}
       10.2,      !- Daily Dry-Bulb Temperature Range {C}
 DefaultMultipliers, !- Dry-Bulb Temperature Range Modifier Type
           ,      !- Dry-Bulb Temperature Range Modifier Day Schedule Name
    Dewpoint,     !- Humidity Condition Type
       17.8,      !- Dewpoint at Maximum Dry-Bulb {C}
           ,      !- Humidity Indicating Day Schedule Name
           ,      !- Humidity Ratio at Maximum Dry-Bulb {kgWater/kgDryAir}
           ,      !- Enthalpy at Maximum Dry-Bulb {J/kg}
           ,      !- Daily Wet-Bulb Temperature Range {deltaC}
     93783.,      !- Barometric Pressure {Pa}
       3.35,   !- [N/A] Wind Speed {m/s} traditional 3.35 m/s (7mph)
          0,   !- [N/A] Wind Direction {Degrees; N=0, S=180}
         No,      !- Rain {Yes/No}
         No,      !- Snow on ground {Yes/No}
         No,      !- Daylight Savings Time Indicator
  ASHRAEClearSky, !- Solar Model Indicator
           ,      !- Beam Solar Day Schedule Name
           ,      !- Diffuse Solar Day Schedule Name
           ,      !- ASHRAE Clear Sky Optical Depth for Beam Irradiance (taub)
           ,      !- ASHRAE Clear Sky Optical Depth for Diffuse Irradiance (taud)
       1.00;      !- Clearness {0.0 to 1.1}
 
