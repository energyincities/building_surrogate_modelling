import matplotlib
import matplotlib.pyplot as plt
# Load Model:
import pickle
from tensorflow.keras.models import model_from_json
from scipy.stats import pearsonr, norm
import GPy
import climin

import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.metrics import r2_score

matplotlib.rcParams['mathtext.fontset'] = 'custom'
matplotlib.rcParams['mathtext.rm'] = 'Bitstream Vera Sans'
matplotlib.rcParams['mathtext.it'] = 'Bitstream Vera Sans:italic'
matplotlib.rcParams['mathtext.bf'] = 'Bitstream Vera Sans:bold'
matplotlib.rc('text', usetex=False)
from mpl_toolkits.axes_grid1.inset_locator import (inset_axes, InsetPosition, mark_inset)


###### Error metrics #################

def mean_absolute_percentage_error(y_true, y_pred):
    y_true, y_pred = np.array(y_true), np.array(y_pred)
    return np.mean(np.abs((y_true - y_pred) / y_true)) * 100

def absolute_percentage_error(y_true, y_pred):
    y_true, y_pred = np.array(y_true), np.array(y_pred)
    return np.abs((y_true - y_pred) / y_true) * 100

def percentile_error(y_true, y_pred, percentile=90):
    return np.percentile(abs(y_true-y_pred)/y_true, percentile, axis=0)*100


###### Helper functions for uncertainty quality assessment #################

def compute_interval(p, sigma):
    p_two_tail = 1 - (1-p)/2
    return norm.ppf(p_two_tail)*sigma

def count_frequencies(p, sigma, mean, y):
    freq=0
    for i in range(len(y)):
        interval = compute_interval(p, sigma[i])
        if (y[i]>(mean[i]-interval)) & (y[i]<(mean[i]+interval)):
            freq+=1
    return freq/len(y)


labels = ['Heating supply, Gas [MWh]', 'Heating supply, Elec. [MWh]', 'Heating supply, Other [MWh]',
          'Cooling supply, Elec. [MWh]', 'Interior lights [MWh]', 'Interior equipment [MWh]',
          'Fans [MWh]', 'Pumps [MWh]', 'Water heating, Gas [MWh]', 'PV Generation [MWh]',
          'Heating demand [MWh]', 'Cooling demand [MWh]']


###### Main class #################

class performance_evaluator:
    def __init__(self, model_path = None, keras_model = False):
        """
        This object is provided to evaluate the performance of the different Bayesian (or deterministic)
        approaches considered in this study.


        :param model_path: Path to the model without suffix (model (.json file), weights (.h5), accompanying files (.p)).
        :param keras_model: Binary to flag if considered model is a Keras model. This changes the way this
        class loads the model.
        """
        self.model_path = model_path
        self.keras_model = keras_model

        if self.keras_model:
            _, _, _, _, self.transformer_inputs, self.transformer_outputs = pickle.load(open(model_path + '.p', 'rb'))
            print("Loaded preprocessing transformers.")

            # load model architecture
            json_file = open(model_path + '.json', 'r')
            loaded_model_json = json_file.read()
            json_file.close()
            self.model = model_from_json(loaded_model_json)

            # load weights into new model
            self.model.load_weights(model_path + ".h5")
            print("Loaded model from disk")

        else:
            pass

    def invers_trafo(self, y):
        return np.exp(self.transformer_outputs.inverse_transform(y))

    def predict(self, X, MC=False):
        if MC == True:
            y_arr = [self.model.predict(self.transformer_inputs.transform(X), verbose=False) for i in range(40)]
            self.y_hat = np.mean(y_arr, axis=0)
            self.y_std = np.std(y_arr, axis=0)
        else:
            self.y_hat = self.model.predict(self.transformer_inputs.transform(X), verbose=False)


    def compute_accuracy(self, X, y, r2=True, mape=True, ae90=True, **kwargs):
        if 'MC' in kwargs.keys():
            self.predict(X, MC=kwargs['MC'])
        else:
            self.predict(X)
        delta_y = abs(y - self.y_hat)

        # calculate errors
        if r2:
            self.r2 = r2_score(y, self.invers_trafo(self.y_hat), multioutput='raw_values')
        if ae90:
            self.ae90 = percentile_error(y, self.invers_trafo(self.y_hat))
        if mape:
            error_test = absolute_percentage_error(y, self.invers_trafo(self.y_hat))
            # Compute MAPE errors after removing outliers
            self.mape = [error_test[:, i][error_test[:, i] < 100].mean() for i in range(error_test.shape[1])]



    def compute_uncertainty_calibration(self, X, y, sharpness=False, auc=False, **kwargs):
        if 'MC' in kwargs.keys():
            self.predict(X, MC=kwargs['MC'])
        else:
            self.predict(X)
        # calculate uncertainty quality metrics
        self.aucs = []
        self.sharpnesses = []
        for cols in range(y.shape[1]):
            r = np.arange(0,100,5)
            self.freqs = [count_frequencies(p/100, self.y_std[:,cols], 
                                            self.y_hat[:,cols], 
                                            self.transformer_outputs.transform(np.log(y))[:,cols]) for p in r]
            self.aucs.append(sum(abs(np.array(self.freqs) - r/100)))
            self.sharpnesses.append(np.mean(self.y_std[:,cols]))

    def draw_calibration_plot(self):
        if len(self.aucs)==0:
            print("First run compute_uncertainty_calibration function.")
        else:
            fig, ax = plt.subplots(4, 3, figsize=(8, 8))
            j = 0
            ii = 0

            for cols in range(len(self.aucs)):
                r = np.arange(0, 100, 5)
                ax[ii, j].plot(r / 100, self.freqs, linewidth=4, alpha=.9, )
                ##ax[ii,j].set_xlim([0,1])
                ax[ii, j].set_ylim([0, 1])
                ax[ii, j].plot([0, 1], [0, 1], 'k--')
                ax[ii, j].set_title([l.split('[')[0][:-1] for l in labels][
                                        cols])  # str(np.round(auc,2))+', '+str(np.round(sharpness, 3)))
                if (ii == 3) & (j == 0):
                    ax[ii, j].set_xlabel('Expected confidence\n level')
                    ax[ii, j].set_ylabel('Empirical confidence\n level')

                j += 1
                if j == 3:
                    j = 0
                    ii += 1
            plt.tight_layout()
            #pickle.dump(ax, open('myplot_new.pickle', 'wb'))
            
    def compute_confidence(self, y):
        self.oracle = []
        self.model_order = []
        self.auc_conf = []
        self.err_drop = []
        for ind in range(y.shape[1]):
            delta_y_norm = abs(y - self.y_hat)/ y
            a = np.sort(np.array(delta_y_norm)[:,ind])[::-1]
            b = np.array(delta_y_norm)[:,ind][np.argsort(self.y_std[:,ind])[::-1]]

            self.oracle.append( [np.mean(a[i:])*100 for i in range(len(a))] )
            self.model_order.append( [np.mean(b[i:])*100 for i in range(len(a))] )


            # Compute Area under the curve
            self.auc_conf.append( np.round(np.sum(np.array(b)-np.array(a))) )

            # Compute error drop
            self.err_drop.append( np.round(b[0]-b[300],2) )# Assuming that 10% of the errors can be discarded
            print(f"Error dropped by {self.err_drop[-1]} when filtering out the first 10\%.")
        
    def apply_filtering(self, fraction=0.1):
        self.index = np.argsort(self.y_std[:,ind])[::-1][600:]
        self.y_hat = self.y_hat[index,:]
        self.y_std = self.y_std[index,:]
        print(f'Filtered out the {fraction*100}% most uncertain samples.')
        
    def draw_confidence_plot(self):

        fig, ax = plt.subplots(4,3, figsize=(6,6))
        j=0
        ii=0
        for i in range(self.y_hat.shape[1]):
            ax[ii,j].plot(self.oracle[i],'-')
            ax[ii,j].plot(self.model_order[i],'-')
            #ax.set_xticks(np.arange(0,self.y_hat.shape[0]+300,600))
            #ax.set_xticklabels(np.arange(0,110,20))
            if (ii==3)&(j==0):
                ax[ii,j].set_xlabel('Percentile')
                ax[ii,j].set_ylabel('MAPE [%]')

            j+=1
            if j==3:
                j=0
                ii+=1
        plt.legend(["Ranking based on Var", "Oracle ranking"])
        plt.tight_layout()



        
        
        
        
class performance_evaluator_SVGP(performance_evaluator):
    
    
    def __init__(self,  X_train, y_train, rep, model_path = None):
        _,_,_,_, self.transformer_inputs, self.transformer_outputs = pickle.load(open(model_path + 'metadata.p','rb'))

        
        self.model = []
        for i in range(1,y_train.shape[1]+1):
            D = X_train.shape[1]
            
            Z_load = np.load(model_path + f'Z_{i}_{rep}.npy')
            y_load = np.load(model_path + f'gp_y_{i}_{rep}.npy')
            X_load = np.load(model_path + 'gp_X.npy')

            gp_load = GPy.core.SVGP(X_load, y_load, Z_load,
                          GPy.kern.Matern32(input_dim=D, lengthscale=np.arange(1,D+1), ARD=True)+ GPy.kern.White(input_dim=D),
                          GPy.likelihoods.Gaussian(), batchsize = 100)
            gp_load.kern.white.variance = 1e-5
            gp_load.kern.white.fix()
            gp_load.update_model(False)
            gp_load.initialize_parameter()
            gp_load[:] = np.load(model_path + f'gp_params_{i}_{rep}.npy')
            gp_load.update_model(True)
            
            self.model.append(gp_load)
            
        print("Loaded models (one model per output).")
        
    
    
    
    def predict(self, X):
        self.y_hat = np.array([self.model[i].predict(self.transformer_inputs.transform(X))[0]
                               for i in range(len(self.model))]).squeeze().transpose()
        self.y_std = np.array([self.model[i].predict(self.transformer_inputs.transform(X))[1]
                               for i in range(len(self.model))]).squeeze().transpose()